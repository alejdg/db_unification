Preparation:
1. Dump the schema
1. Create schema
1. Add old_id row:
 1. String or JSON
 1. indicating old_ids in more than 1 db

Execution:
1. Dump old-db (data only)
1. Add old_id values to the dump
1. Check duplicated entries in the DB
1. Insert the new entries
1. Update the already existing ones.

